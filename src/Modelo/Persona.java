/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madar
 */
public class Persona implements Comparable{
    
    private long cedula;
    private String nombre;
    
    //Requisito 1: 

    public Persona() {
    }

    public Persona(long cedula, String nombre) {
        this.cedula = cedula;
        this.nombre = nombre;
    }
    
    
    //Requisito 2 (opcional depende de la clase)

    public long getCedula() {
        return cedula;
    }

    public void setCedula(long cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    //Requisito 3:

    @Override
    public String toString() {
        return "Persona{" + "cedula=" + cedula + ", nombre=" + nombre + '}';
    }
    
    //Requisito 4:

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (int) (this.cedula ^ (this.cedula >>> 32));
        return hash;
    }

    //el método equals viene de la clase Object
    @Override
    public boolean equals(Object obj) {
        if (this == obj) { // Está preguntando si los dos objetos TIENEN la misma dirección de memoria
            return true;
        }
        if (obj == null) { // Está preguntando si el objeto que YO(this) me voy a comparar es null
            return false;
        }
        if (getClass() != obj.getClass()) { // Si los dos objetos pertenecen a la misma clase
            return false;
        }
        // el objeto a comparar se establece como "final" para que el objeto a comparar sea una constante y se asegure que NO van a cambiar el valor de sus atributos
        final Persona other = (Persona) obj; // Realizo el middlerCasting , es decir convierto el object a la clase respectiva
        if (this.cedula != other.cedula) { // Realizo la comparación SEGÚN su atributo
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Object obj) { // resta : obj1==obj2--> 0, obj1>obj2 --> ">0", obj1<obj2 --> "<0"
        
        if (this == obj) { // Está preguntando si los dos objetos TIENEN la misma dirección de memoria
            return 0;
        }
//        if (obj == null) { // Está preguntando si el objeto que YO(this) me voy a comparar es null
//            return false;
//        }
//        if (getClass() != obj.getClass()) { // Si los dos objetos pertenecen a la misma clase
//            return false;
//        }
        // el objeto a comparar se establece como "final" para que el objeto a comparar sea una constante y se asegure que NO van a cambiar el valor de sus atributos
        final Persona other = (Persona) obj; // Realizo el middlerCasting , es decir convierto el object a la clase respectiva
        /*
            ¿Cuando un objeto de la clase persona es ==, <, > ?
                REALIZO LA COMPARACIÓN POR CEDULAS
        */
            if(this.cedula==other.getCedula())
                return 0;
            if(this.cedula>other.getCedula())
                return 1;
            if(this.cedula<other.getCedula())
                return -1;
            
            
       return 0; //<-- Nunca lo va hacer 
    }
    
    
    
    
    
}
