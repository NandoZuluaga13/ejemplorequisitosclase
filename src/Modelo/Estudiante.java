/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 * Requisitos:
 *      1. Un objeto es igual a otro si tienen el mismo valor para todos sus parciales.
 *      2. Un objeto es >,< con el análisis del promedio de sus notas según nuestro reglamento
 * 
 * @author madar
 */
public class Estudiante {
    
    private long codigo;
    private float p1, p2,p3, examen;

    public Estudiante() {
    }

    public Estudiante(long codigo, float p1, float p2, float p3, float examen) {
        this.codigo = codigo;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.examen = examen;
    }

    public long getCodigo() {
        return codigo;
    }

    public float getP1() {
        return p1;
    }

    public float getP2() {
        return p2;
    }

    public float getP3() {
        return p3;
    }

    public float getExamen() {
        return examen;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public void setP1(float p1) {
        this.p1 = p1;
    }

    public void setP2(float p2) {
        this.p2 = p2;
    }

    public void setP3(float p3) {
        this.p3 = p3;
    }

    public void setExamen(float examen) {
        this.examen = examen;
    }
    
    /*
        METODO QUE RETORNA EL PROMEDIO DE LAS NOTAS
    */
    
    public float getPromedio(){
        float promedio=0;
        promedio= (this.p1 + this.p2 + this.p3 + this.examen)/4;
        
        return promedio;
    }
           

    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) 
            return true;
        
        if (obj == null) 
            return false;
        
        if (getClass() != obj.getClass()) 
            return false;
        
        final Estudiante other = (Estudiante) obj;
        
        boolean aux=true;
        
        if(other.getP1()!=this.p1)
            aux=false;
        
        if(other.getP2()!=this.p2)
            aux=false;
        
        if(other.getP3()!=this.p3)
            aux=false;
        
        if(other.getExamen()!=this.examen)
            aux=false;
        
        
        return aux;
    }
    
    public int compareTo(Object obj) { 
        
        if (this == obj)
            return 0;
        
        final Estudiante other = (Estudiante) obj;
        
        int aux=0;
        
        if(this.getPromedio()<other.getPromedio())
            aux=-1;
        
        if(this.getPromedio()>other.getPromedio())
            aux=1;
        
        return aux;
    }
    

    @Override
    public String toString() {
        return "Estudiante{" + "codigo=" + codigo + ", p1=" + p1 + ", p2=" + p2 + ", p3=" + p3 + ", examen=" + examen + '}';
    }
    
    
    
    
}
